=========================
Pilotis IO AWS
=========================

.. image:: https://gitlab.com/ekinox-io/ekinox-libraries/pilotis-io-aws/badges/master/pipeline.svg
    :target: https://gitlab.com/ekinox-io/ekinox-libraries/pilotis-io-aws/pipelines

Implementation of Pilotis IO library for AWS.

* Free software license: MIT

Examples
========

Loading a raw CSV file from the Amazon S3.
Requires the following environment variables:

- `AWS_ACCESS_KEY_ID`: your AWS access key
- `AWS_SECRET_ACCESS_KEY`: your AWS secret key
- `AWS_DEFAULT_REGION`: your AWS default region


.. code-block:: python
   from pilotis_io.s3 import S3IoApi, S3PandasApi
   from pilotis_io.directory_structure import dataset_raw_dir_path

   bucket_name = "my_bucket"
   io_api = S3IoApi(project_dir)
   pandas_api = S3PandasApi(io_api)

   csv_file = dataset_raw_dir_path(
    dataset_name="my_dataset",
    dataset_version="2020_01_01"
    ) / "my_file.csv"
   my_dataframe = pandas_api.load_pandas_dataset([csv], sep=";")


Credits
-------

This package was created with Cookiecutter_ and the
`opinionated-digital-center/python-library-project-generator`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`opinionated-digital-center/python-library-project-generator`: https://github.com/opinionated-digital-center/python-library-project-generator
