from pathlib import Path
from typing import List, Optional

import boto3
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
import pytest
from hamcrest.core import assert_that
from moto import mock_s3
from pandas.util.testing import assert_frame_equal
from pilotis_io.exceptions import PilotisIoError
from pilotis_io.pandas import PandasApi

from pilotis_io_aws.s3 import S3IoAPI, S3PandasAPi

_REGION = "us-east-1"
_BUCKET_NAME = "data-bucket"


@pytest.fixture
def pandas_api(tmp_path: Path) -> PandasApi:
    io_api = S3IoAPI(bucket_name=_BUCKET_NAME)
    return S3PandasAPi(io_api)


def _init_bucket():
    conn = boto3.resource("s3", region_name=_REGION)
    conn.create_bucket(Bucket=_BUCKET_NAME)


@mock_s3
def test_save_pandas_dataset_csv(pandas_api: PandasApi):
    # Given a pandas dataset
    _init_bucket()
    df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})

    # When saving it to CSV
    export_path: Path = Path("export") / Path("file.csv")
    pandas_api.save_pandas_dataset(df, export_path)

    # Then a CSV file must exists
    assert_that(pandas_api.io_api.file_exists(export_path))


@mock_s3
def test_save_pandas_dataset_parquet(pandas_api: PandasApi):
    # Given a pandas dataset
    _init_bucket()
    df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})

    # When saving it to CSV
    export_path: Path = Path("export") / Path("file.parquet")
    pandas_api.save_pandas_dataset(df, export_path)

    # Then a CSV file must exists
    assert_that(pandas_api.io_api.file_exists(export_path))


@mock_s3
def test_save_pandas_dataset_unknown_format(pandas_api: PandasApi):
    # Given a pandas dataset
    _init_bucket()
    df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})

    # When saving it to a non supported format
    # Then it should raise an exception
    export_path: Path = Path("export") / Path("file.unknown")
    with pytest.raises(PilotisIoError):
        pandas_api.save_pandas_dataset(df, export_path)

    # And no file is generated
    assert_that(not pandas_api.io_api.file_exists(export_path))


@mock_s3
def test_save_pandas_dataset_with_no_dataset(pandas_api: PandasApi):
    # Given None as the dataset
    _init_bucket()
    df: Optional[pd.DataFrame] = None

    # When saving it to CSV
    # Then it should raise an exception
    export_path: Path = Path("export") / Path("file.csv")
    with pytest.raises(PilotisIoError):
        pandas_api.save_pandas_dataset(df, export_path)

    # And no file is generated
    assert_that(not pandas_api.io_api.file_exists(export_path))


@mock_s3
def test_save_pandas_dataset_with_no_destination_path(pandas_api: PandasApi):
    # Given None as the dataset
    df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})

    # When saving it to a non set destination
    # Then it should raise an exception
    export_path: Optional[Path] = None
    with pytest.raises(PilotisIoError):
        pandas_api.save_pandas_dataset(df, export_path)


@mock_s3
@pytest.mark.skip(reason="Reading file using S3Fs not supported by moto")
def test_load_pandas_dataset_with_one_csv_file(pandas_api: PandasApi, tmp_path: Path):
    # Given a CSV file inside the API's directory
    _init_bucket()
    csv_relative_path = Path("export.csv")
    csv_local_path = tmp_path / csv_relative_path
    csv_local_path.write_text("col1,col2\n1,3\n2,4\n")
    pandas_api.io_api.store_file(csv_local_path, csv_relative_path)

    # When loading it as a pandas DF
    df = pandas_api.load_pandas_dataset(relative_file_paths=[csv_relative_path])

    # Then the data is in memory
    expected_df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})
    assert_frame_equal(expected_df, df)


@mock_s3
@pytest.mark.skip(reason="Reading file using S3Fs not supported by moto")
def test_load_pandas_dataset_with_two_csv_file(pandas_api: PandasApi, tmp_path: Path):
    # Given two CSV files inside the API's directory
    _init_bucket()
    csv_1_relative_path = Path("export_1.csv")
    csv_1_local_path = tmp_path / csv_1_relative_path
    csv_1_local_path.write_text(data="col1,col2\n1,3\n")
    pandas_api.io_api.store_file(csv_1_local_path, csv_1_relative_path)
    csv_2_relative_path = Path("export_1.csv")
    csv_2_local_path = tmp_path / csv_2_relative_path
    csv_2_local_path.write_text(data="col1,col2\n1,3\n")
    pandas_api.io_api.store_file(csv_2_local_path, csv_2_relative_path)

    # When loading it as a pandas DF
    df = pandas_api.load_pandas_dataset(
        relative_file_paths=[csv_1_relative_path, csv_2_relative_path]
    )

    # Then the data is in memory
    expected_df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})
    assert_frame_equal(expected_df.reset_index(drop=True), df.reset_index(drop=True))


@mock_s3
@pytest.mark.skip(reason="Reading file using S3Fs not supported by moto")
def test_load_pandas_dataset_with_one_parquet_file(
    pandas_api: PandasApi, tmp_path: Path
):
    # Given a CSV file inside the API's directory
    _init_bucket()
    parquet_relative_path = Path("export.parquet")
    parquet_local_path = tmp_path / parquet_relative_path
    table = pa.Table.from_pydict(mapping={"col1": [1, 2], "col2": [3, 4]})  # noqa
    pq.write_table(table=table, where=str(parquet_local_path))

    # When loading it as a pandas DF
    df = pandas_api.load_pandas_dataset(relative_file_paths=[parquet_relative_path])

    # Then the data is in memory
    expected_df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})
    assert_frame_equal(expected_df, df)


@mock_s3
def test_load_pandas_dataset_with_one_unknown_format_file(
    pandas_api: PandasApi, tmp_path: Path
):
    # Given a CSV file with an unknown format
    _init_bucket()
    csv_relative_path = Path("export.my_custom_csv")
    csv_local_path = tmp_path / csv_relative_path
    csv_local_path.write_text(data="col1,col2\n1,3\n2,4\n")
    pandas_api.io_api.store_file(csv_local_path, csv_relative_path)

    # When loading it as a pandas DF
    # Then it should raise an error
    with pytest.raises(PilotisIoError):
        pandas_api.load_pandas_dataset(relative_file_paths=[csv_relative_path])


def test_load_pandas_dataset_with_no_file_to_load(pandas_api: PandasApi):
    # Given an empty list of files to load
    relative_paths: List[Path] = []

    # When loading it as a pandas DF
    # Then it should raise an error
    with pytest.raises(PilotisIoError):
        pandas_api.load_pandas_dataset(relative_file_paths=relative_paths)


def test_load_pandas_dataset_with_none_as_the_files_to_load(
    pandas_api: PandasApi,
):
    # Given an null list of files to load
    relative_paths: Optional[List[Path]] = None

    # When loading it as a pandas DF
    # Then it should raise an error
    with pytest.raises(PilotisIoError):
        pandas_api.load_pandas_dataset(relative_file_paths=relative_paths)
