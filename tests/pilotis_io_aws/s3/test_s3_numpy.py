from pathlib import Path
from typing import Optional

import boto3
import numpy as np
import pytest
from hamcrest.core import assert_that
from moto import mock_s3
from numpy.testing import assert_array_equal
from pilotis_io.exceptions import PilotisIoError
from pilotis_io.numpy import NumpyApi

from pilotis_io_aws.s3 import S3IoAPI, S3NumpyApi

_REGION = "us-east-1"
_BUCKET_NAME = "data-bucket"


@pytest.fixture
def numpy_api(tmp_path: Path) -> NumpyApi:
    io_api = S3IoAPI(bucket_name=_BUCKET_NAME)
    return S3NumpyApi(io_api)


def _init_bucket():
    conn = boto3.resource("s3", region_name=_REGION)
    conn.create_bucket(Bucket=_BUCKET_NAME)


@mock_s3
def test_save_numpy_array_in_npz_format(numpy_api: NumpyApi, tmp_path: Path):
    _init_bucket()

    # Given a numpy array
    arr = np.array([1, 2, 3, 4, 5])

    # When saving it as NPZ
    relative_export_path = Path("export.npz")
    numpy_api.save_numpy_array(arr, relative_export_path=relative_export_path)

    # Then it creates a file
    assert_that(numpy_api.io_api.file_exists(relative_export_path))

    # And the file content is readable using numpy lib
    relative_copy_path = tmp_path / Path("export.copy.npz")
    numpy_api.io_api.copy_or_symlink_to_local(relative_export_path, relative_copy_path)
    loaded_arr = np.load(str(relative_copy_path))["arr_0"]
    assert_array_equal(arr, loaded_arr)


def test_save_numpy_array_in_unknown_format(numpy_api: NumpyApi):
    # Given a numpy array
    arr = np.array([1, 2, 3, 4, 5])

    # When saving it as an unknown format
    # Then it should raise an error
    relative_export_path = Path("export.unknown_format")
    with pytest.raises(PilotisIoError):
        numpy_api.save_numpy_array(arr, relative_export_path=relative_export_path)


@mock_s3
def test_save_numpy_array_with_none_as_the_array(numpy_api: NumpyApi):
    _init_bucket()

    # Given a numpy array
    arr: Optional[np.ndarray] = None

    # When saving it as an unknown format
    # Then it should raise an error
    relative_export_path = Path("export.unknown_format")
    with pytest.raises(PilotisIoError):
        numpy_api.save_numpy_array(arr, relative_export_path=relative_export_path)

    # And no file should be created
    assert_that(not numpy_api.io_api.file_exists(relative_export_path))


def test_save_numpy_array_with_none_as_the_export_path(numpy_api: NumpyApi):
    # Given a numpy array
    arr = np.array([1, 2, 3, 4, 5])

    # When saving it as an unknown format
    # Then it should raise an error
    relative_export_path: Optional[Path] = None
    with pytest.raises(PilotisIoError):
        numpy_api.save_numpy_array(arr, relative_export_path=relative_export_path)


@mock_s3
def test_load_numpy_array_with_a_npz_file(numpy_api: NumpyApi, tmp_path: Path):
    _init_bucket()

    # Given a numpy NPZ file inside the API's directory
    relative_path = Path("file.npz")
    relative_local_path = tmp_path / relative_path
    arr = np.array([1, 2, 3, 4, 5])
    np.savez(str(relative_local_path), arr)
    numpy_api.io_api.store_file(relative_local_path, relative_path)

    # When loading it
    loaded_arr = numpy_api.load_numpy_array(relative_path)

    # Then it should get the data in memory
    assert_array_equal(arr, loaded_arr)


def test_load_numpy_array_none_as_path(numpy_api: NumpyApi):
    # Given a None path
    relative_path: Optional[Path] = None

    # When loading it
    # Then it should raise an error
    with pytest.raises(PilotisIoError):
        numpy_api.load_numpy_array(relative_path)


@mock_s3
def test_load_numpy_array_with_a_path_that_does_not_exists_should_throw_an_error(
    numpy_api: NumpyApi,
):
    _init_bucket()

    # Given a None path
    relative_path = Path("file.npz")

    # When loading it
    # Then it should raise an error
    with pytest.raises(PilotisIoError):
        numpy_api.load_numpy_array(relative_path)
