from pathlib import Path

import boto3
import pytest
from hamcrest import assert_that, contains_exactly, equal_to
from moto import mock_s3
from pilotis_io.exceptions import PilotisIoError

from pilotis_io_aws.s3 import S3IoAPI

_REGION = "us-east-1"
_BUCKET_NAME = "data-bucket"


@pytest.fixture
def s3_api() -> S3IoAPI:
    return S3IoAPI(bucket_name=_BUCKET_NAME)


def _init_bucket():
    conn = boto3.resource("s3", region_name=_REGION)
    conn.create_bucket(Bucket=_BUCKET_NAME)


@mock_s3
def test_get_path_uri(s3_api: S3IoAPI):
    # Given a file
    file = Path("test.txt")

    # When
    url = s3_api.get_path_uri(file)

    # Then
    assert_that(url, equal_to(f"s3://{s3_api.bucket_name}/{str(file)}"))


@mock_s3
def test_get_path_uri_when_no_file_provided(s3_api: S3IoAPI):
    # When / Then
    with pytest.raises(PilotisIoError):
        s3_api.get_path_uri(None)


@mock_s3
def test_file_exists_when_file_exists(s3_api: S3IoAPI):
    # Given a file placed in the API's bucket
    _init_bucket()
    s3 = boto3.client("s3", region_name=_REGION)
    s3.put_object(Bucket=s3_api.bucket_name, Key="test.txt", Body="File content")

    # When testing file existence
    result = s3_api.file_exists(Path("test.txt"))

    # Then it should detect it
    assert_that(result)


@mock_s3
def test_file_exists_when_file_does_not_exists(s3_api: S3IoAPI):
    # Given a file placed in the API's bucket
    _init_bucket()

    # When testing file existence
    result = s3_api.file_exists(Path("test.txt"))

    # Then it should detect it
    assert_that(not result)


@mock_s3
def test_file_exists_when_no_path_provided(s3_api: S3IoAPI):
    # Given a file placed in the API's bucket
    _init_bucket()

    # When / Then
    with pytest.raises(PilotisIoError):
        s3_api.file_exists(None)


@mock_s3
def test_store_file(s3_api: S3IoAPI, tmp_path: Path):
    # Given
    _init_bucket()
    local_file_path = tmp_path / "file.txt"
    file_content = "file content"
    local_file_path.write_text(file_content)
    relative_output_path = Path("target.txt")

    # When
    s3_api.store_file(local_file_path, relative_output_path)

    # Then
    s3 = boto3.client("s3", region_name=_REGION)
    res = s3.get_object(Bucket=s3_api.bucket_name, Key="target.txt")
    assert_that(res["ContentLength"], equal_to(len(file_content)))


@mock_s3
def test_store_file_when_target_already_exists(s3_api: S3IoAPI, tmp_path: Path):
    # Given
    _init_bucket()
    local_file_path = tmp_path / "file.txt"
    file_content = "file content"
    local_file_path.write_text(file_content)
    relative_output_path = Path("target.txt")
    s3_api.store_file(local_file_path, relative_output_path)

    # When / Then
    with pytest.raises(PilotisIoError):
        s3_api.store_file(local_file_path, relative_output_path)


@mock_s3
def test_store_file_when_no_local_file(s3_api: S3IoAPI):
    # Given
    _init_bucket()
    relative_output_path = Path("target.txt")

    # When / Then
    with pytest.raises(PilotisIoError):
        s3_api.store_file(None, relative_output_path)


@mock_s3
def test_store_file_when_no_target_path(s3_api: S3IoAPI, tmp_path: Path):
    # Given
    _init_bucket()
    local_file_path = tmp_path / "file.txt"
    file_content = "file content"
    local_file_path.write_text(file_content)

    # When / Then
    with pytest.raises(PilotisIoError):
        s3_api.store_file(local_file_path, None)


@mock_s3
def test_copy_or_symlink_to_local(s3_api: S3IoAPI, tmp_path: Path):
    # Given
    _init_bucket()
    local_file_path = tmp_path / "file.txt"
    file_content = "file content"
    local_file_path.write_text(file_content)
    relative_path = Path("file.txt")
    s3_api.store_file(local_file_path, relative_path)
    local_copy_path = tmp_path / "file2.txt"

    # When
    s3_api.copy_or_symlink_to_local(relative_path, local_copy_path)

    # Then
    assert_that(local_file_path.read_text(), equal_to(local_copy_path.read_text()))


@mock_s3
def test_copy_or_symlink_to_local_with_no_path_to_download(
    s3_api: S3IoAPI, tmp_path: Path
):
    # Given
    _init_bucket()
    local_copy_path = tmp_path / "file.txt"

    # When
    with pytest.raises(PilotisIoError):
        s3_api.copy_or_symlink_to_local(None, local_copy_path)


@mock_s3
def test_copy_or_symlink_to_local_with_no_target_path(s3_api: S3IoAPI, tmp_path: Path):
    # Given
    _init_bucket()
    local_file_path = tmp_path / "file.txt"
    file_content = "file content"
    local_file_path.write_text(file_content)
    relative_path = Path("file.txt")
    s3_api.store_file(local_file_path, relative_path)

    # When
    with pytest.raises(PilotisIoError):
        s3_api.copy_or_symlink_to_local(relative_path, None)


@mock_s3
def test_list_files_in_dir(s3_api: S3IoAPI, tmp_path: Path):
    # Given
    _init_bucket()
    bucket_dir = Path("directory")
    local_file_path = tmp_path / "file.txt"
    local_file_path.touch()
    copy_1_path = bucket_dir / "copy_1.txt"
    s3_api.store_file(local_file_path, copy_1_path)
    copy_2_path = bucket_dir / "copy_2.txt"
    s3_api.store_file(local_file_path, copy_2_path)

    # When
    files = s3_api.list_files_in_dir(bucket_dir)

    # Then
    assert_that(files, contains_exactly(copy_1_path, copy_2_path))


@mock_s3
def test_list_files_in_dir_when_no_dir(s3_api: S3IoAPI):
    # Given
    _init_bucket()

    # When
    with pytest.raises(PilotisIoError):
        s3_api.list_files_in_dir(None)
